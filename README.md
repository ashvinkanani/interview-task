# Getting Started

### Retrieve tweets

1. Register user
2. Login as user
3. Fetch twitter tweets by providing 'q' and 'place' parameter.

### Retrieve facebook posts

1. Register user
2. Login as user
3. Generate facebook authorization url.
4. Retrieve facebook auth code using authorization url.
5. Retrieve access token using auth code
6. Fetch facebook posts

### Docs

- [Architecture](wiki/architecture.md)
- [Future enhancements](wiki/future-enhancements.md)
- [FAQs](wiki/faqs.md)


# Basic authentication

- Stateless basic authentication using cookie is provided.
- Whenever user logins, a `JSESSIONID` cookie is set to browser.
- This cookie value is updated whenever user invokes authenticated APIs.

# Retrieve social feeds

- An interface [SocialFeedsService] is provided. One can provide as many implementations of this interface.
- This service provides two implementations of it.
  - [TwitterFeedsService]
  - [FacebookFeedsService]

# Retrieve facebook access token

- To retrieve facebook posts, we need access token of an user whose posts we want to fetch.
- To achieve this, we have provided 2 APIs
  - One to retrieve facebook authorization url.
  - One to retrieve accesstoken from auth code.
- Both APIs can be accessed by logged in user only.
- When we invoke first API, it results authorization url. Copy this authorization URL and open it in browser.
- This will ask facebook credentials to provide access to the "facebook app".
- Login with facebook credentials and you are redirected to facebook callback url with authcode.
- Invoking this API as logged in user will retireve accessToken from authcode and stores in in-memory store.

## Components

- [FacebookController]
- [FacebookServiceImpl]

# Error handling

- To handle REST API errors, we have created [BaseApiController].
- Each rest controller should extend this class and service layer should throw any exception defined in `com.jrd.socialfeeds.exception` package.
- This will show proper error code and message to user when REST API fails.

[SocialFeedsService]: ../src/main/java/com/jrd/socialfeeds/feeds/SocialFeedsService.java
[TwitterFeedsService]: ../src/main/java/com/jrd/socialfeeds/feeds/TwitterFeedsService.java
[FacebookFeedsService]: ../src/main/java/com/jrd/socialfeeds/feeds/FacebookFeedsService.java
[FacebookController]: ../src/main/java/com/jrd/socialfeeds/feeds/facebook/FacebookController.java
[FacebookServiceImpl]: ../src/main/java/com/jrd/socialfeeds/feeds/facebook/FacebookServiceImpl.java
[BaseApiController]: ../src/main/java/com/jrd/socialfeeds/config/BaseApiController.java

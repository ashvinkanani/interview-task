# FAQs

## For twitter, how place (country or city name) is resolved to "woeid" ("Where On Earth IDentifier") ?

- We have configured a json file which contains `woeid` mapping for country and city names world wide.
- This json file is read by a component [WoeidProvider].
- This component also provides method to retrieve `woeid` for a given place.
- When fetching tweets from twitter, this component is used to replace original place with respective `woeid`.

## How to configure new service to retrieve feeds from it ?

Follow steps below to configure new service

- Create new configuration class that configures respective connection factory used to communicate with service.
  - e.g. [TwitterConfiguration]
- Now, provide implementation of an interface [SocialFeedsService]
- Enhance [SocialFeedsServiceProvider.getService] and [SocialServiceImpl.getType] to support new service.
- Configure configuration properties and perform testing.

[WoeidProvider]: ../src/main/java/com/jrd/socialfeeds/feeds/WoeidProvider.java
[TwitterConfiguration]: ../src/main/java/com/jrd/socialfeeds/feeds/TwitterConfiguration.java
[SocialFeedsService]: ../src/main/java/com/jrd/socialfeeds/feeds/SocialFeedsService.java
[SocialFeedsServiceProvider]: ../src/main/java/com/jrd/socialfeeds/feeds/SocialFeedsServiceProvider.java
[SocialServiceImpl]: ../src/main/java/com/jrd/socialfeeds/feeds/SocialServiceImpl.java
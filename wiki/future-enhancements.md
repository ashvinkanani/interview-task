# Future enhancements

## Facebook authentication

- To retrieve facebook feeds, for now user need to authenticate himself twice. Once using login API and 2nd time using facebook authorization url.
- So, we can enable facebook login along with basic authentication. So, user don't need to perform two steps.

## Storing facebook access token in databse

- For now when, facebook user authorizes himself, application stores the access token detail as in-memory store.
- This doesn't help to survive application restart.
- So, we can store this access token detail in database which can be loaded again on service restart.

## Avoid fetching duplicate feeds

- As you can see, [SocialServiceImpl] makes an extra query in database to fetch all feeds with specific ids.
- And avoids saving feed again which is already stored in database.
- This can be simply avoided by enhancing [SocialFeedsService] implementations to fetch the feeds since the last fetched feed id.
- This doesn't require any logic to fetch duplicate feeds as we will already retrieve new feeds.

[SocialFeedsService]: ../src/main/java/com/jrd/socialfeeds/feeds/SocialFeedsService.java
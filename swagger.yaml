swagger: "2.0"

info:
  description: "This document describes the APIs related to user and social feeds."
  version: "1.0.0"
  title: "Social feeds"

host: "localhost:8080"

basePath: "/"

tags:
- name: "user"
  description: "Operations about user"
- name: "Social feed"
  description: "Operatios about social feeds like tweets and facebook posts"
- name: "Facebook login"
  description: "Operations about retrieving facebook user's access token to retreive facebook posts."

schemes:
- "http"

paths:

  /user:
    post:
      tags:
      - "user"
      summary: "Create user"
      description: "Creates/registers new application user."
      operationId: "createUser"
      consumes:
      - "application/json"
      parameters:
      - in: "body"
        name: "body"
        description: "Created user object"
        required: true
        schema:
          $ref: "#/definitions/User"
      responses:
        400:
          schema:
            $ref: "#/definitions/ErrorInfo"
          description: |-
            - Username is not provided.
            - Password is not provided.
            - Password is < 6 characters long.
        409:
          schema:
            $ref: "#/definitions/ErrorInfo"
          description: "User with provided username (case insensitive) is already exists."
        201:
          description: "User is created successfully."
        default:
          description: "unknown server error"
  
  /login:
    post:
      tags:
      - "user"
      summary: "Do user login"
      description: |-
        - Logins a particular user with given 'username' in to the system.
        - As a response, Cookie will be set in user's browser.
      operationId: "doLogin"
      consumes:
      - "application/json"
      parameters:
      - in: "body"
        name: "body"
        required: true
        schema:
          $ref: "#/definitions/LoginRequest"
      responses:
        400:
          schema:
            $ref: "#/definitions/ErrorInfo"
          description: |-
            - Username is not provided.
            - Password is not provided.
            - Incorrect username or password.
        200:
          description: "User is logged in successfully."
        default:
          description: "unknown server error"
  
  /feeds/{service}:
    get:
      tags:
      - "Social feed"
      summary: "Fetch social feeds."
      description: |-
        - Fetches social feeds from a particular service for a given place and query. And store fetched feeds to database.
        - In case fetched feeds are found duplicate (already fetched and store) for logged in user, then skips duplicate feeds.
        - The feeds stored in database as current logged in user can only be fetched by him.
        
        ### Authorization
        - Only logged in user can access this API.

      operationId: "fetchFeed"
      parameters:
      - name: "service"
        in: "path"
        type: "string"
        enum:
        - twitter
        - facebook
        description: The service from which feeds is to fetch.
        required: true
      - name: "q"
        in: "query"
        type: "string"
        description: The query or phrase to search. When 'service=facebook', it will be ignored.
      - name: "place"
        in: "query"
        type: "string"
        description: The name of the country or city. When 'service=facebook', it will be ignored.
      responses:
        400:
          description: |-
            - Service is not provided.
            - Service is other than 'twitter' and 'facebook'.
        403:
          description: "Logged in user is not found."
        200:
          description: "Feeds are fetched successfully."
        default:
          description: "unknown server error"
  
  /feeds:
    get:
      tags:
      - "Social feed"
      summary: "List feeds"
      description: |-
        - Lists social feeds of logged in user.
        - When "type" is not provided explicitly, then all type of feeds is returned.
        - Otherwise, particular type of feeds are only returned.
        - The API supports pagination and the default page size is 10.
        
        ### Authorization
        - Only logged in user can access this API.
      
      operationId: "listFeeds"
      parameters:
      - name: "type"
        in: "query"
        type: "string"
        enum:
        - TWEET
        - FACEBOOK_FEEDS
        description: The type of feeds to be returned.
        required: true
      - name: "page"
        in: "query"
        type: "integer"
        description: The page to be returned. When not available, first page is returned.
      - name: "pageSize"
        in: "query"
        type: "integer"
        default: 10
        description: The number of records to be returned per page.
      responses:
        400:
          description: If page or pageSize is zero or negative.
        403:
          description: "Logged in user is not found."
        200:
          description: "Feeds are fetched successfully."
          schema:
            $ref: "#/definitions/PageResponse"
        default:
          description: "unknown server error"
  
  /facebook/generateauthurl:
    get:
      tags:
      - "Facebook login"
      summary: "Retrieve facebook authorize url."
      description: |-
        - Returns authorize url of the facebook.
        - Once returned, open this url in browser and login using facebook credentials.
        - This will return url to generate access token using auth code returned by facebook.
        
        ### Authorization
        - Only logged in user can access this API.
      
      operationId: "generateauthurl"
      produces:
      - text/plain
      responses:
        403:
          description: "Logged in user is not found."
        200:
          description: The facebook authorize url.
  
  /facebook/callback:
    get:
      tags:
      - "Facebook login"
      summary: "Retrieve access token"
      description: |-
        - Retrieves facebook user's accessToken using given code.
        
        ### Authorization
        - Only logged in user can access this API.
      
      operationId: "getAccessToken"
      responses:
        403:
          description: "Logged in user is not found."
        200:
          description: Access token is retrieved successfully.

definitions:

  User:
    type: object
    required:
      - username
      - password
    properties:
      firstName:
        type: string
        maxLength: 250
      lastName:
        type: string
        maxLength: 250
      username:
        type: string
        description: "Unique username."
        maxLength: 250
      password:
        type: string
        format: password
        minLength: 6
        maxLength: 250
    example:
      firstName: Ashvin
      lastName: Kanani
      username: ashvin.kanani
      password: 123456
  
  LoginRequest:
    type: object
    required:
      - username
      - password
    properties:
      username:
        type: string
      password:
        type: string
        format: password
    example:
      username: ashvin.kanani
      password: 123456
  
  PageResponse:
    type: object
    properties:
      content:
        type: array
        items:
          $ref: "#/definitions/SocialFeed"
      totalRecords:
        type: number
        description: The total amount of records.
      totalPages:
        type: number
        description: The number of total pages based on 'pageSize' and 'totalRecords'.
      pageSize:
        type: number
        description: Current page size.
      numberOfRecords:
        type: number
        description: The number of records in current page.
  
  SocialFeed:
    type: object
    properties:
      id:
        type: string
        description: The Unique ID.
      userId:
        type: string
        description: The id of the user who fetched this feed.
      type:
        type: string
        enum:
        - TWEET
        - FACEBOOK_FEEDS
      feedId:
        type: string
        description: The unique id of the feed provided by social site. e.g. twitter, facebook, etc.
      text:
        type: string
        description: The feed text.
      displayName:
        type: string
        description: Display name of the social user.
      internalName:
        type: string
        description: Social User account name.
      createdAt:
        type: number
        description: Feed creation time.
  
  ErrorInfo:
    type: object
    properties:
      code:
        type: string
        description: The error code. e.g. BAD_REQUEST
      message:
        type: string
        description: The error message. e.g. Please, verify that username is provided.
        
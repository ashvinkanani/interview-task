package com.jrd.socialfeeds.users;

import com.jrd.socialfeeds.exception.IllegalArgumentException;
import com.jrd.socialfeeds.exception.IllegalStateException;
import com.jrd.socialfeeds.model.User;

import org.springframework.web.bind.annotation.RequestBody;

/**
 * A component that manages application users.
 * 
 * @author Ashvin
 */
public interface UserService {

  /**
   * Creates/registers new application user.
   * 
   * @param user {@link User}.
   * @throws IllegalArgumentException If 'user' is <code>null</code>.
   * @throws IllegalArgumentException If username or password is <code>null</code>, blank or
   *         whitespace.
   * @throws IllegalArgumentException If password is less than 6 characters long.
   * @throws IllegalArgumentException If username or password is more than 250 characters long.
   * @throws IllegalStateException If username (case insensitive) is already in use.
   */
  public void create(@RequestBody User user);

}

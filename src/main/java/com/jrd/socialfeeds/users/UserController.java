package com.jrd.socialfeeds.users;

import com.jrd.socialfeeds.config.BaseApiController;
import com.jrd.socialfeeds.config.Endpoint;
import com.jrd.socialfeeds.exception.IllegalArgumentException;
import com.jrd.socialfeeds.exception.IllegalStateException;
import com.jrd.socialfeeds.model.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * Provides APIs for managing users.
 * 
 * @author Ashvin
 */
@RestController
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class UserController extends BaseApiController {

  @Autowired
  private UserService userService;

  /**
   * Creates/registers new application user.
   * 
   * @param user {@link User}.
   * @throws IllegalArgumentException If username or password is <code>null</code>, blank or
   *         whitespace.
   * @throws IllegalArgumentException If password is less than 6 characters long.
   * @throws IllegalArgumentException If username or password is more than 250 characters long.
   * @throws IllegalStateException If username (case insensitive) is already in use.
   */
  @RequestMapping(value = Endpoint.USERS, method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_JSON_VALUE)
  @ResponseStatus(HttpStatus.CREATED)
  public void create(@RequestBody User user) {
    userService.create(user);
  }

}

package com.jrd.socialfeeds.users;

import com.jrd.socialfeeds.exception.IllegalArgumentException;
import com.jrd.socialfeeds.exception.IllegalStateException;
import com.jrd.socialfeeds.model.User;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(propagation = Propagation.MANDATORY)
public class UserServiceImpl implements UserService {

  private static Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

  private static final String BAD_REQUEST = "BAD_REQUEST";
  private static final String USERNAME_IN_USE = "USERNAME_IN_USE";

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private BCryptPasswordEncoder bcryptPasswordEncoder;

  @Override
  public void create(User user) {

    validateForCreate(user);
    validateUniqueUsername(user.getUsername());

    user.setUsername(StringUtils.trim(user.getUsername())
                                .toLowerCase());
    user.setPassword(bcryptPasswordEncoder.encode(StringUtils.trim(user.getPassword())));

    user.setCreatedAt(new Date());
    user.setUpdatedAt(new Date());

    user = userRepository.save(user);
    logger.info("create() :: Created. username={}", user.getUsername());
  }

  private void validateForCreate(User user) {

    if (user == null) {
      throw new IllegalArgumentException(BAD_REQUEST, "Please, verify that 'user' is provided.");
    }

    if (StringUtils.isBlank(user.getUsername())) {
      throw new IllegalArgumentException(BAD_REQUEST, "Please, verify that 'username' is provided");
    }

    if (StringUtils.isBlank(user.getPassword())) {
      throw new IllegalArgumentException(BAD_REQUEST, "Please, verify that 'password' is provided");
    }

    if (StringUtils.length(user.getPassword()) < 6) {
      throw new IllegalArgumentException(BAD_REQUEST,
          "Please, verify that 'password' is >= 6 charaters long.");
    }
  }

  private void validateUniqueUsername(String username) {
    User user = userRepository.findByUsername(username.toLowerCase());

    if (user != null) {
      throw new IllegalStateException(USERNAME_IN_USE,
          "An user with username: '" + username + "' already exists.");
    }
  }
}

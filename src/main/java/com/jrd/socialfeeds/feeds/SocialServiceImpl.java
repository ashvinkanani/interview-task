package com.jrd.socialfeeds.feeds;

import com.jrd.socialfeeds.exception.AccessDeniedException;
import com.jrd.socialfeeds.exception.IllegalArgumentException;
import com.jrd.socialfeeds.feeds.dto.PageResponse;
import com.jrd.socialfeeds.model.SocialFeed;
import com.jrd.socialfeeds.model.SocialFeed.Type;
import com.jrd.socialfeeds.model.User;
import com.jrd.socialfeeds.security.SecurityService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class SocialServiceImpl implements SocialService {

  private static final Logger logger = LoggerFactory.getLogger(SocialServiceImpl.class);

  @Autowired
  private SocialFeedsServiceProvider socialFeedsServiceProvider;

  @Autowired
  private SocialFeedRepository socialFeedRepository;

  @Autowired
  private SecurityService securityService;

  @Override
  public Map<String, Integer> search(String service, String query, String place) {

    if (StringUtils.isEmpty(service)) {
      throw new IllegalArgumentException("BAD_REQUEST",
          "Please, verify that 'service' i21s provided.");
    }

    User user = validateAndGetUser();

    // Fetch new social feeds
    SocialFeedsService socialFeedService = socialFeedsServiceProvider.getService(service);
    List<SocialFeed> feeds = socialFeedService.search(place, query);

    Map<String, Integer> response = new HashMap<>();
    response.put("fetched", feeds.size());

    // Find duplicate and exclude them from being saved in database
    List<String> duplicateFeedIds = getDuplicateFeedIds(user.getId(), service, feeds);
    response.put("duplicate", duplicateFeedIds.size());

    List<SocialFeed> newFeeds = feeds.stream()
                                     .filter(feed -> !duplicateFeedIds.contains(feed.getFeedId()))
                                     .collect(Collectors.toList());

    // Set logged in user id
    newFeeds.forEach((feed) -> {
      feed.setUserId(user.getId());
    });

    if (CollectionUtils.isEmpty(newFeeds)) {
      logger.info(
          "search() :: No new feeds found. userId={}, service={}; place={}; query={}; count={}",
          user.getId(), service, place, query, newFeeds.size());
      return response;
    }

    socialFeedRepository.insert(newFeeds);
    response.put("saved", newFeeds.size());
    logger.info("search() :: Completed. userId={}; service={}; place={}; query={}; count={}",
        user.getId(), service, place, query, newFeeds.size());
    return response;
  }

  private User validateAndGetUser() {
    User user = securityService.getCurrentUser();
    if (user == null) {
      throw new AccessDeniedException("UNAUTHORIZED", "No logged in user is found.");
    }
    return user;
  }

  /**
   * Returns duplicate feed ids from given new feeds for the given user and service. <br/>
   * TODO: We can remove this logic by fetching later on feeds since last feeds. But, due to time
   * constraint for now I (Ashvin) have preferred this solution.
   */
  private List<String> getDuplicateFeedIds(String userId, String service,
      List<SocialFeed> newFeeds) {

    List<String> newFeedIds = new ArrayList<>();
    newFeeds.forEach((feed) -> {
      newFeedIds.add(feed.getFeedId());
    });

    Type type = getType(service);
    List<SocialFeed> existingFeeds =
        socialFeedRepository.findByUserIdAndTypeAndFeedIdIn(userId, type, newFeedIds);
    if (CollectionUtils.isEmpty(existingFeeds)) {
      logger.debug("getDuplicateFeedIds() :: No duplicate entries found. userId={}; service={}");
      return new ArrayList<>();
    }

    List<String> duplicateFeedIds = new ArrayList<>();
    existingFeeds.forEach((feed) -> {
      duplicateFeedIds.add(feed.getFeedId());
    });
    logger.info(
        "getDuplicateFeedIds() :: {} duplicate feeds found. userId={}; service={}; feedIds={}",
        userId, duplicateFeedIds.size(), service, duplicateFeedIds);
    return duplicateFeedIds;
  }

  private Type getType(String service) {

    switch (service) {
      case "twitter":
        return Type.TWEET;

      case "facebook":
        return Type.FACEBOOK_FEEDS;

      default:
        throw new IllegalArgumentException("INVALID_SERVICE",
            "Service Not Supported. Valid services are 'twitter' and 'facebook'.");
    }
  }

  @Override
  public PageResponse<SocialFeed> list(Type type, Integer page, Integer pageSize) {
    validateForList(page, pageSize);

    User user = validateAndGetUser();

    if (page == null) {
      page = 1;
    }
    if (pageSize == null) {
      pageSize = 10;
    }

    Pageable pageable = PageRequest.of(page - 1, pageSize);
    Page<SocialFeed> result = null;
    if (type == null) {
      result = socialFeedRepository.findByUserId(user.getId(), pageable);
    } else {
      result = socialFeedRepository.findByUserIdAndType(user.getId(), type, pageable);
    }
    logger.info("list() :: Completed. userId={}; type={}; page={}; count={}", user.getId(), type,
        page, result.getNumberOfElements());
    return toPageResponse(result);
  }

  private void validateForList(Integer page, Integer pageSize) {
    if (page != null && page < 1) {
      throw new IllegalArgumentException("BAD_REQUEST", "'page' can not be < 1.");
    }

    if (pageSize != null && pageSize < 1) {
      throw new IllegalArgumentException("BAD_REQUEST", "'pageSize' can not be < 1.");
    }
  }

  private PageResponse<SocialFeed> toPageResponse(Page<SocialFeed> result) {
    PageResponse<SocialFeed> response = new PageResponse<>();

    response.setContent(result.getContent());
    response.setTotalRecords(result.getTotalElements());
    response.setTotalPages(result.getTotalPages());
    response.setPageSize(result.getPageable()
                               .getPageSize());
    response.setNumberOfRecords(result.getNumberOfElements());

    return response;
  }
}

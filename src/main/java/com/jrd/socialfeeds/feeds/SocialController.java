package com.jrd.socialfeeds.feeds;

import com.jrd.socialfeeds.config.BaseApiController;
import com.jrd.socialfeeds.config.Endpoint;
import com.jrd.socialfeeds.exception.AccessDeniedException;
import com.jrd.socialfeeds.exception.IllegalArgumentException;
import com.jrd.socialfeeds.feeds.dto.PageResponse;
import com.jrd.socialfeeds.model.SocialFeed;
import com.jrd.socialfeeds.model.SocialFeed.Type;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Provides APIs related to managing (e.g. fetch, list, etc.) social feeds (e.g. tweets, facebook
 * posts, etc.).
 * 
 * @author Ashvin
 */
@RestController
public class SocialController extends BaseApiController {

  @Autowired
  private SocialService socialService;

  /**
   * Fetches social feeds from a particular service for a given place and query. And store fetched
   * feeds to database. <br/>
   * In case fetched feeds are found duplicate (already fetched and store) for logged in user, then
   * skips duplicate feeds.
   * 
   * @param service The service from which feeds is to fetch. Possible values are 'twitter' and
   *        'facebook'.
   * @param query The query or phrase to search.
   * @param place The name of the country or city.
   * @throws IllegalArgumentException If service is <code>null</code>, blank or whitespace.
   * @throws IllegalArgumentException If service is other than 'twitter' or 'facebook'.
   * @throws AccessDeniedException If logged in user is not found.
   */
  @RequestMapping(value = Endpoint.FETCH_FEEDS, produces = MediaType.APPLICATION_JSON_VALUE)
  public Map<String, Integer> search(@PathVariable String service,
      @RequestParam(name = "q", required = false) String query,
      @RequestParam(name = "place", required = false) String place) {
    return socialService.search(service, query, place);
  }

  /**
   * Lists social feeds of logged in user. <br/>
   * When "type" is not provided explicitly, then all type of feeds is returned. Otherwise,
   * particular type of feeds are only returned. <br/>
   * The default page size is 10.
   * 
   * @param type The type of feeds to be returned.
   * @param page The page to be returned. When not available, first page is returned.
   * @param pageSize The number of records to be returned per page. Defaults to 10.
   * @return {@link List}.
   * @throws IllegalArgumentException if page or pageSize is zero or negative.
   * @throws AccessDeniedException If logged in user is not found.
   */
  @RequestMapping(value = Endpoint.LIST_FEEDS, produces = MediaType.APPLICATION_JSON_VALUE)
  public PageResponse<SocialFeed> list(@RequestParam(name = "type", required = false) Type type,
      @RequestParam(name = "page", required = false) Integer page,
      @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize) {
    return socialService.list(type, page, pageSize);
  }
}

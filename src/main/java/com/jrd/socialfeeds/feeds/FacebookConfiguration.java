package com.jrd.socialfeeds.feeds;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;

/**
 * Configures facebook instances to communicate with twitter.
 * 
 * @author Ashvin
 */
@Configuration
public class FacebookConfiguration {

  @Value("${spring.social.facebook.app-id}")
  private String facebookAppId;

  @Value("${spring.social.facebook.app-secret}")
  private String facebookAppSecret;

  /**
   * Creates {@link FacebookConnectionFactory} instance used to communicate with facebook.
   */
  @Bean
  public FacebookConnectionFactory facebookConnectionFactory() {
    return new FacebookConnectionFactory(facebookAppId, facebookAppSecret);
  }
}

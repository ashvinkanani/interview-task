package com.jrd.socialfeeds.feeds;

import com.jrd.socialfeeds.model.SocialFeed;
import com.jrd.socialfeeds.model.SocialFeed.Type;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SocialFeedRepository extends MongoRepository<SocialFeed, String> {

  /**
   * Searches social feeds by type and given feedIds for a particular user.
   */
  List<SocialFeed> findByUserIdAndTypeAndFeedIdIn(String userId, Type type, List<String> feedIds);

  /**
   * Returns pageable list of social feeds of a particular user.
   */
  Page<SocialFeed> findByUserId(String userId, Pageable pageable);

  /**
   * Returns pageable list of social feeds by type of a particular user.
   */
  Page<SocialFeed> findByUserIdAndType(String userId, Type type, Pageable pageable);
}

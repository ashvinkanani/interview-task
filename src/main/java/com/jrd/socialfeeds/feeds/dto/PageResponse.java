package com.jrd.socialfeeds.feeds.dto;

import java.util.List;

public class PageResponse<T> {

  /**
   * The page content as {@link List}.
   */
  private List<T> content;

  /**
   * The total amount of records.
   */
  private long totalRecords;

  /**
   * The number of total pages based on {@link #pageSize} and {@link #totalRecords}.
   */
  private int totalPages;

  /**
   * Current page size.
   */
  private long pageSize;

  /**
   * The number of records in current page.
   */
  private int numberOfRecords;

  public List<T> getContent() {
    return content;
  }

  public void setContent(List<T> content) {
    this.content = content;
  }

  public long getTotalRecords() {
    return totalRecords;
  }

  public void setTotalRecords(long totalRecords) {
    this.totalRecords = totalRecords;
  }

  public int getTotalPages() {
    return totalPages;
  }

  public void setTotalPages(int totalPages) {
    this.totalPages = totalPages;
  }

  public long getPageSize() {
    return pageSize;
  }

  public void setPageSize(long pageSize) {
    this.pageSize = pageSize;
  }

  public int getNumberOfRecords() {
    return numberOfRecords;
  }

  public void setNumberOfRecords(int numberOfRecords) {
    this.numberOfRecords = numberOfRecords;
  }
}

package com.jrd.socialfeeds.feeds;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

/**
 * Provides "Where On Earth IDentifier" (woeid) for the given country or city name (place).
 * 
 * @author Ashvin
 */
@Component
public class WoeidProvider {

  @Value("${twitter.woeidJsonPath:classpath:woeid.json}")
  private String woeidJsonPath;

  @Autowired
  private ResourceLoader resourceLoader;

  @Autowired
  private ObjectMapper objectMapper;

  private Map<String, Integer> woeidByPlace = null;

  /**
   * Provides "Where On Earth IDentifier" (woeid) for the given country or city name (place). <br/>
   * Returns <code>null</code> when no woeid found for provided place.
   */
  public Integer get(String place) {
    return getWoeIds().get(StringUtils.lowerCase(place));
  }

  /**
   * Returns woeids read from JSON file configured using "twitter.woeidJsonPath" configuration
   * properties. <br/>
   * If woeids are not already read from configured JSON file, then this method reads it and
   * initializes {@link #woeidByPlace}.
   */
  private synchronized Map<String, Integer> getWoeIds() {

    if (woeidByPlace != null) {
      return woeidByPlace;
    }

    try {
      Resource resource = resourceLoader.getResource(woeidJsonPath);

      CollectionType valueType = objectMapper.getTypeFactory()
                                             .constructCollectionType(List.class, Woeid.class);

      List<Woeid> woeids = objectMapper.readValue(resource.getInputStream(), valueType);

      woeidByPlace = new HashMap<>();
      woeids.forEach(woeid -> {
        // Converted to lower case to make search easy.
        woeidByPlace.put(woeid.getName()
                              .toLowerCase(),
            woeid.getWoeid());
      });

      return woeidByPlace;
    } catch (Exception ex) {
      throw new RuntimeException("Faield to read Awoeids from JSON file. path=" + woeidByPlace, ex);
    }
  }

  public static class Woeid {

    /**
     * The name of the Country or City.
     */
    private String name;

    /**
     * The woeid for this Country or City.
     */
    private Integer woeid;

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public Integer getWoeid() {
      return woeid;
    }

    public void setWoeid(Integer woeid) {
      this.woeid = woeid;
    }
  }
}

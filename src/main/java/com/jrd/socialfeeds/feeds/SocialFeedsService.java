package com.jrd.socialfeeds.feeds;

import com.jrd.socialfeeds.model.SocialFeed;

import java.util.List;

/**
 * Provides Java APIs to search (fetch) social feeds.
 * 
 * @author Ashvin
 */
public interface SocialFeedsService {

  /**
   * Searches social feeds (tweets, facebook posts, etc.) for given place and query. <br/>
   * TODO: Search API can be enhanced to fetch newer feeds only since last fetched feed.
   */
  List<SocialFeed> search(String place, String query);

}

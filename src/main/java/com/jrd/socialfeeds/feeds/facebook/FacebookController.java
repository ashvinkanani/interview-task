package com.jrd.socialfeeds.feeds.facebook;

import com.jrd.socialfeeds.config.BaseApiController;
import com.jrd.socialfeeds.config.Endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Provides APIs to login as facebook user to retrieve this user's feeds.
 * 
 * @author Ashvin
 */
@RestController
public class FacebookController extends BaseApiController {

  @Autowired
  private FacebookService facebookService;

  /**
   * Returns authorize url of the facebook. Once returned, open this url in browser and login using
   * facebook credentials. This will return url to generate access token using auth code returned by
   * facebook.
   * 
   * @return The authorization url.
   */
  @GetMapping(Endpoint.FACEBOOK_GET_AUTH_URL)
  public String generateAuthorizeUrl() {
    return facebookService.generateAuthroizeUrl();
  }

  /**
   * Retrieves facebook user's accessToken using given code.
   * 
   * @param code The auth code returned by facebook authorize url.
   * @throws IllegalArgumentException If code is <code>null</code>, blank or whitespace.
   */
  @GetMapping(Endpoint.FACEBOOK_CALLBACK)
  public void generateAccessToken(@RequestParam("code") String code) {
    facebookService.generateAccessToken(code);
  }
}

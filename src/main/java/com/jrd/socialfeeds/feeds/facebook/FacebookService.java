package com.jrd.socialfeeds.feeds.facebook;

import com.jrd.socialfeeds.exception.AccessDeniedException;

public interface FacebookService {

  /**
   * Returns authorize url of the facebook. Once returned, open this url in browser and login using
   * facebook credentials. This will return url to generate access token using auth code returned by
   * facebook.
   * 
   * @return The authorization url.
   */
  public String generateAuthroizeUrl();

  /**
   * Retrieves facebook user's accessToken using given code.
   * 
   * @param code The auth code returned by facebook authorize url.
   * @throws IllegalArgumentException If code is <code>null</code>, blank or whitespace.
   */
  public void generateAccessToken(String code);

  /**
   * Returns logged in user's facebook accessToken, if available.
   * 
   * @return The facebook accessToken for logged in user.
   * @throws AccessDeniedException If accessToken is not available.
   */
  public String getAccessToken();

  /**
   * Returns facebook user id corresponding to the logged in user, if available.
   * 
   * @return The facebook user id for logged in user.
   * @throws AccessDeniedException If facebook user id is not available.
   */
  public String getFacebookUserId();
}

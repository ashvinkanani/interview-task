package com.jrd.socialfeeds.feeds.facebook;

import com.jrd.socialfeeds.config.Endpoint;
import com.jrd.socialfeeds.exception.AccessDeniedException;
import com.jrd.socialfeeds.exception.IllegalArgumentException;
import com.jrd.socialfeeds.model.User;
import com.jrd.socialfeeds.security.SecurityService;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;
import org.springframework.social.oauth2.OAuth2Parameters;
import org.springframework.stereotype.Service;

/**
 * Provides methods to login as facebook user to retrieve this user's feeds.
 * 
 * @author Ashvin
 */
@Service
public class FacebookServiceImpl implements FacebookService {

  private static final Logger logger = LoggerFactory.getLogger(FacebookServiceImpl.class);

  private static final String REDIRECT_URL = "http://localhost:8080" + Endpoint.FACEBOOK_CALLBACK;

  private static final String[] fields = {"id", "first_name", "name"};

  /**
   * Local (in-memory) facebook accessToken store. For now it won't survive service restart. We can
   * store it as cache on disk or in database to survive service restart.
   */
  private Map<String, String> accessTokenByUserId = new HashMap<>();

  /**
   * Local (in-memory) facebook userIds store.
   */
  private Map<String, String> facebookIdByUserId = new HashMap<>();

  @Autowired
  private FacebookConnectionFactory facebookConnectionFactory;

  @Autowired
  private SecurityService securityService;

  @Override
  public String generateAuthroizeUrl() {
    OAuth2Parameters params = new OAuth2Parameters();
    params.setRedirectUri(REDIRECT_URL);
    params.setScope("email");
    return facebookConnectionFactory.getOAuthOperations()
                                    .buildAuthenticateUrl(params);
  }

  @SuppressWarnings("rawtypes")
  @Override
  public void generateAccessToken(String code) {
    if (StringUtils.isBlank(code)) {
      throw new IllegalArgumentException("BAD_REQUEST", "Please, verify that code is provided.");
    }

    User user = validateAndGetUser();

    String accessToken = facebookConnectionFactory.getOAuthOperations()
                                                  .exchangeForAccess(code, REDIRECT_URL, null)
                                                  .getAccessToken();
    accessTokenByUserId.put(user.getId(), accessToken);

    // Retrieve basic user profile data and store it's id in in-memory store
    Map userInfo = getUserData(accessToken);
    String facebookUserId = (String) userInfo.get("id");
    facebookIdByUserId.put(user.getId(), facebookUserId);

    logger.info("generateAccessToken() :: Retrieved. userId={}; userInfo={}", user.getId(),
        userInfo);
  }

  private User validateAndGetUser() {
    User user = securityService.getCurrentUser();
    if (user == null) {
      throw new AccessDeniedException("UNAUTHORIZED", "No logged in user is found.");
    }
    return user;
  }

  /**
   * Retrieves basic facebook user profile using given accessToken.
   */
  @SuppressWarnings("rawtypes")
  private Map getUserData(String accessToken) {
    Facebook facebook = new FacebookTemplate(accessToken);
    return facebook.fetchObject("me", Map.class, fields);
  }

  @Override
  public String getAccessToken() {
    User user = validateAndGetUser();

    String accessToken = accessTokenByUserId.get(user.getId());

    if (StringUtils.isEmpty(accessToken)) {
      throw new AccessDeniedException("UNAUTHORIZED",
          "You have not authorized facebook account yet.");
    }

    logger.debug("getAccessToken() :: Found. userId={}", user.getId());
    return accessToken;
  }

  @Override
  public String getFacebookUserId() {
    User user = validateAndGetUser();

    String facebookUserId = facebookIdByUserId.get(user.getId());

    if (StringUtils.isEmpty(facebookUserId)) {
      throw new AccessDeniedException("UNAUTHORIZED",
          "You have not authorized facebook account yet.");
    }

    logger.debug("getFacebookUserId() :: Found. userId={}; facebookUserId={}", user.getId(),
        facebookUserId);
    return facebookUserId;
  }
}

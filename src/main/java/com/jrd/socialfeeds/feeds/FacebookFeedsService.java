package com.jrd.socialfeeds.feeds;

import com.jrd.socialfeeds.exception.AccessDeniedException;
import com.jrd.socialfeeds.feeds.facebook.FacebookService;
import com.jrd.socialfeeds.model.SocialFeed;
import com.jrd.socialfeeds.model.SocialFeed.Type;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.PagedList;
import org.springframework.social.facebook.api.Post;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.stereotype.Service;

@Service
public class FacebookFeedsService implements SocialFeedsService {

  private static final Logger logger = LoggerFactory.getLogger(FacebookFeedsService.class);

  @Autowired
  private FacebookService facebookService;

  /**
   * Returns facebook posts/feeds. Both "place" and "query" will be ignored.
   * 
   * @throws AccessDeniedException If user has not authorized facebook.
   */
  @Override
  public List<SocialFeed> search(String place, String query) {
    String accessToken = facebookService.getAccessToken();
    // String ownerId = facebookService.getFacebookUserId();

    Facebook facebook = new FacebookTemplate(accessToken);
    PagedList<Post> facebookPosts = facebook.feedOperations()
                                            .getPosts();

    // TODO: These lines are never tested as without verifying facebook app for Business we are not
    // able to retrieve user's posts.
    List<SocialFeed> posts = new ArrayList<>();
    for (Post post : facebookPosts) {
      posts.add(toFeed(post));
    }
    logger.info("search() :: Completed.");
    return posts;
  }

  private SocialFeed toFeed(Post post) {
    SocialFeed feed = new SocialFeed();

    feed.setType(Type.FACEBOOK_FEEDS);
    feed.setFeedId("" + post.getId());
    feed.setText(post.getMessage());
    feed.setDisplayName(post.getAdminCreator()
                            .getName());
    feed.setInternalName(post.getAdminCreator()
                             .getId());
    feed.setCreatedAt(post.getCreatedTime()
                          .getTime());
    return feed;
  }

}

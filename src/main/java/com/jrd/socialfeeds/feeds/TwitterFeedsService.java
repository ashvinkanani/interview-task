package com.jrd.socialfeeds.feeds;

import com.jrd.socialfeeds.exception.IllegalArgumentException;
import com.jrd.socialfeeds.exception.ResourceNotFoundException;
import com.jrd.socialfeeds.model.SocialFeed;
import com.jrd.socialfeeds.model.SocialFeed.Type;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import twitter4j.HttpResponseCode;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Trend;
import twitter4j.Trends;
import twitter4j.Twitter;
import twitter4j.TwitterException;

@Service
public class TwitterFeedsService implements SocialFeedsService {

  private static final Logger logger = LoggerFactory.getLogger(TwitterFeedsService.class);

  private static final String DEFAULT_QUERY = "Education";

  @Autowired
  private Twitter twitter;

  @Autowired
  private WoeidProvider woeidProvider;

  /**
   * Returns top 10 trending tweets for given place and/or query.<br/>
   * This method performs below steps: <br/>
   * 1. When place is provided, retrieve woeid for given place. <br/>
   * 2. If woeid is found, then retrieves top 2 trending topic for that woeid. <br/>
   * 3. And then retrieves top 10 trending tweets for these topics with given query filter.
   * 
   * @throws IllegalArgumentException If place is provided but it's woeid is not found.
   */
  @Override
  public List<SocialFeed> search(String place, String query) {

    String urlEncodedQuery = getTwitterQuery(place, query);

    return execute(() -> {
      Query twitterQuery = new Query(urlEncodedQuery);
      twitterQuery.setCount(10);

      QueryResult result = twitter.search(twitterQuery);

      List<SocialFeed> tweets = new ArrayList<>();
      for (Status tweet : result.getTweets()) {
        tweets.add(toFeed(tweet));
      }
      logger.info("search() :: Completed. query={}; place={}", urlEncodedQuery, place);
      return tweets;
    }, "search. place=" + place + "; query=" + query);
  }

  /**
   * Returns query in the format "{query} {#topic1} OR {#topic2}".
   */
  private String getTwitterQuery(String place, String query) {

    Map<String, String> topics = getTrendingTopics(place);

    if (MapUtils.isEmpty(topics)) {
      return StringUtils.trimToEmpty("");
    }

    return getQuery(query, topics);
  }

  private String getQuery(String query, Map<String, String> topics) {

    query = StringUtils.isEmpty(query) ? DEFAULT_QUERY : query;
    StringBuilder sb = new StringBuilder(query);
    if (StringUtils.isNotBlank(query)) {
      sb.append(" "); // Space between query and topics.
    }

    int count = topics.size() > 2 ? 2 : topics.size();
    int idx = 1;
    for (Map.Entry<String, String> entry : topics.entrySet()) {
      sb.append(entry.getValue());

      if (idx < count) {
        sb.append(" OR ");
      }

      if (idx == count) {
        break;
      }
      idx++;
    }

    return sb.toString();
  }

  /**
   * Returns top trending topics for given place.
   * 
   * @param place The country or city name.
   * @return {@link Map}. key=Topic name; Value=URL encoded query.
   */
  private Map<String, String> getTrendingTopics(String place) {
    if (StringUtils.isBlank(place)) {
      return new HashedMap<>();
    }

    Integer woeid = woeidProvider.get(place.trim()
                                           .toLowerCase());
    if (woeid == null) {
      throw new IllegalArgumentException("INVALID_PLACE",
          "Please, make sure that given place '" + place + "' is valid.");
    }

    return execute(() -> {

      Trends trends = twitter.getPlaceTrends(woeid);
      if (ArrayUtils.isEmpty(trends.getTrends())) {
        return new HashedMap<>();
      }

      Map<String, String> trendingTopics = new HashedMap<>();
      for (Trend trend : trends.getTrends()) {
        trendingTopics.put(trend.getName(), trend.getQuery());
      }

      logger.info("getTrendingTopics() :: Completed. place={}; count={}", place,
          trendingTopics.size());
      return trendingTopics;
    }, "retrieve trending topics. place=" + place);
  }

  private SocialFeed toFeed(Status tweet) {
    SocialFeed feed = new SocialFeed();

    feed.setType(Type.TWEET);
    feed.setFeedId("" + tweet.getId());
    feed.setText(tweet.getText());
    feed.setDisplayName(tweet.getUser()
                             .getName());
    feed.setInternalName(tweet.getUser()
                              .getScreenName());
    feed.setCreatedAt(tweet.getCreatedAt()
                           .getTime());
    return feed;
  }


  /**
   * Executes given task and handles related errors.
   */
  private <T> T execute(Callable<T> task, String action) {
    try {

      return task.call();

    } catch (TwitterException ex) {
      logger.info("Failed to perform twitter operation. action={}", action, ex);

      // if (ex.exceededRateLimitation()) {
      // throw new IllegalStateException("RATE_LIMIT_EXCEEDED",
      // "Too many requests. Please, try after some times.");
      // }

      if (ex.isCausedByNetworkIssue()) {
        throw new IllegalArgumentException("NETWORK_ERROR",
            "Network error, please try again later.");
      }

      if (ex.resourceNotFound()) {
        throw new ResourceNotFoundException("NOT_FOUND", "Requested resource is not found.");
      }

      if (ex.getStatusCode() == HttpResponseCode.BAD_REQUEST) {
        throw new IllegalArgumentException("BAD_REQUETS", ex.getMessage());
      }

      throw new RuntimeException("Failed to perform twitter operation. action=" + action, ex);
    } catch (Exception ex) {
      throw new RuntimeException("Failed to execute task. action=" + action, ex);
    }
  }
}

package com.jrd.socialfeeds.feeds;

import com.jrd.socialfeeds.exception.AccessDeniedException;
import com.jrd.socialfeeds.exception.IllegalArgumentException;
import com.jrd.socialfeeds.feeds.dto.PageResponse;
import com.jrd.socialfeeds.model.SocialFeed;
import com.jrd.socialfeeds.model.SocialFeed.Type;

import java.util.Map;

import org.springframework.data.domain.Page;

/**
 * A service that manages social feeds.
 * 
 * @author Ashvin
 */
public interface SocialService {

  /**
   * Fetches social feeds from a particular service for a given place and query. And store fetched
   * feeds to database. <br/>
   * In case fetched feeds are found duplicate (already fetched and store) for logged in user, then
   * skips duplicate feeds.
   * 
   * @param service The service from which feeds is to fetch. Possible values are 'twitter' and
   *        'facebook'.
   * @param query Optional. The query or phrase to search.
   * @param place Optional. The name of the country or city.
   * @throws IllegalArgumentException If service or query is <code>null</code>, blank or whitespace.
   * @throws IllegalArgumentException If service is other than 'twitter' or 'facebook'.
   * @throws AccessDeniedException If logged in user is not found.
   */
  public Map<String, Integer> search(String service, String query, String place);

  /**
   * Lists social feeds of logged in user. <br/>
   * When "type" is not provided explicitly, then all type of feeds is returned. Otherwise,
   * particular type of feeds are only returned. <br/>
   * The default page size is 10.
   * 
   * @param type The type of feeds to be returned.
   * @param page The page to be returned. When not available, first page is returned.
   * @param pageSize The number of records to be returned per page. Defaults to 10.
   * @return {@link Page}.
   * @throws IllegalArgumentException if page or pageSize is zero or negative.
   * @throws AccessDeniedException If logged in user is not found.
   */
  public PageResponse<SocialFeed> list(Type type, Integer page, Integer pageSize);

}

package com.jrd.socialfeeds.feeds;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Configures twitter4j instances to communicate with twitter.
 * 
 * @author Ashvin
 */
@Configuration
public class TwitterConfiguration {

  /**
   * Creates {@link TwitterFactory} instance used to retrieve {@link Twitter} instance.
   */
  @Bean
  public TwitterFactory twitterFactory(TwitterProperties properties) {
    ConfigurationBuilder cb = new ConfigurationBuilder();

    cb.setDebugEnabled(true)
      .setOAuthConsumerKey(properties.getApiKey())
      .setOAuthConsumerSecret(properties.getApiSecret())
      .setOAuthAccessToken(properties.getAccessToken())
      .setOAuthAccessTokenSecret(properties.getAccessTokenSecret());

    return new TwitterFactory(cb.build());
  }

  /**
   * Creates {@link Twitter} instance used to communicate with twitter.
   */
  @Bean
  public Twitter twitter(TwitterFactory twitterFactory) {
    return twitterFactory.getInstance();
  }

}

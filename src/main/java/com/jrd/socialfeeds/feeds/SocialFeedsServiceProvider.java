package com.jrd.socialfeeds.feeds;

import com.jrd.socialfeeds.exception.IllegalArgumentException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Provides {@link SocialFeedsService} implementation based on the "service" name provided.
 * 
 * @author Ashvin
 */
@Component
public class SocialFeedsServiceProvider {

  @Autowired
  private TwitterFeedsService twitterFeedsService;

  @Autowired
  private FacebookFeedsService facebookFeedsService;

  /**
   * Provides {@link SocialFeedsService} implementation based on the "service" name provided.
   * 
   * @param service The name of the service for which {@link SocialFeedsService} instance is to
   *        retrieve.
   * @return {@link SocialFeedsService}.
   * @throws IllegalArgumentException If given service is not supported.
   */
  public SocialFeedsService getService(String service) {

    assert service != null;

    switch (service) {
      case "twitter":
        return twitterFeedsService;

      case "facebook":
        return facebookFeedsService;

      default:
        throw new IllegalArgumentException("INVALID_SERVICE",
            "Service Not Supported. Valid services are 'twitter' and 'facebook'.");
    }
  }

}

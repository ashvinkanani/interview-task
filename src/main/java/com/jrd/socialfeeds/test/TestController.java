package com.jrd.socialfeeds.test;

import com.jrd.socialfeeds.config.BaseApiController;
import com.jrd.socialfeeds.config.Endpoint;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Contains REST APIs for testing purpose.
 * 
 * @author Ashvin
 */
@RestController
public class TestController extends BaseApiController {

  @RequestMapping(path = Endpoint.TEST, method = RequestMethod.GET)
  public String helloWorld() {
    return "Hello World !!";
  }

}

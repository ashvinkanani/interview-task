package com.jrd.socialfeeds.security;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

import java.util.Date;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.authentication.AuthenticationTrustResolverImpl;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.context.HttpRequestResponseHolder;
import org.springframework.security.web.context.SaveContextOnUpdateOrErrorResponseWrapper;
import org.springframework.security.web.context.SecurityContextRepository;

/**
 * Configures strategy used for persisting a {@link SecurityContext} between REST API requests.
 * 
 * @author Ashvin
 */
public class JwtSecurityContextRepository implements SecurityContextRepository {

  private static final Logger logger = LoggerFactory.getLogger(JwtSecurityContextRepository.class);

  private UserDetailsService userDetailsService;

  private String cookieName;
  private String secret;

  private int expiryDuration;

  /**
   * Instantiates {@link JwtSecurityContextRepository}.
   */
  public JwtSecurityContextRepository(UserDetailsService userDetailsService, String cookieName,
      String secret, int expityDuration) {
    this.userDetailsService = userDetailsService;
    this.cookieName = cookieName;
    this.secret = secret;
    this.expiryDuration = expityDuration;
  }

  /**
   * Returns security context for the given request. <br/>
   * Returns empty security context for unauthenticated user.
   */
  @Override
  public SecurityContext loadContext(HttpRequestResponseHolder requestResponseHolder) {
    HttpServletRequest request = requestResponseHolder.getRequest();
    HttpServletResponse response = requestResponseHolder.getResponse();

    SaveToCookieResponseWrapper wrappedResponse =
        new SaveToCookieResponseWrapper(request, response, cookieName, secret, expiryDuration);
    requestResponseHolder.setResponse(wrappedResponse);

    SecurityContext context = readSecurityContextFromCookie(request);

    if (context == null) {
      logger.debug("No SecurityContext was available from the HttpServletRequest. "
          + "A new one will be created.");
      return SecurityContextHolder.createEmptyContext();
    }

    return context;
  }

  private SecurityContext readSecurityContextFromCookie(HttpServletRequest request) {
    Cookie[] cookies = request.getCookies();
    if (ArrayUtils.isEmpty(cookies)) {
      logger.debug("No Cookie exists currently.");
      return null;
    }

    for (Cookie cookie : cookies) {
      if (!cookie.getName()
                 .equals(cookieName)) {
        continue;
      }

      // Cookie exists, so try to obtain a context from it.
      return readSecurityContext(cookie);
    }

    logger.debug("No '{}' Cookie exists in request.", cookieName);
    return null;
  }

  private SecurityContext readSecurityContext(Cookie cookie) {
    try {
      String username = getUsername(cookie);
      UserDetails userDetails = this.userDetailsService.loadUserByUsername(username);

      SecurityContext context = SecurityContextHolder.createEmptyContext();
      context.setAuthentication(new UsernamePasswordAuthenticationToken(userDetails.getUsername(),
          userDetails.getPassword(), userDetails.getAuthorities()));

      if (logger.isDebugEnabled()) {
        logger.debug("Obtained a valid SecurityContext: '{}' from Cookie.", context);
      }
      return context;
    } catch (ExpiredJwtException ex) {
      if (logger.isDebugEnabled()) {
        logger.debug("Authentication Cookie is expired.", ex);
      }
      return null;
    } catch (UnsupportedJwtException | MalformedJwtException | SignatureException
        | IllegalArgumentException ex) {
      logger.warn("Tampered jwt Authentication Cookie detected.", ex);
      return null;
    }
  }

  private String getUsername(Cookie cookie) {
    return Jwts.parser()
               .setSigningKey(secret)
               .parseClaimsJws(cookie.getValue())
               .getBody()
               .getSubject();
  }

  @Override
  public void saveContext(SecurityContext context, HttpServletRequest request,
      HttpServletResponse response) {
    SaveToCookieResponseWrapper responseWrapper = (SaveToCookieResponseWrapper) response;
    if (!responseWrapper.isContextSaved()) {
      responseWrapper.saveContext(context);
    }
  }

  @Override
  public boolean containsContext(HttpServletRequest request) {
    return readSecurityContextFromCookie(request) != null;
  }

  /**
   * The response wrapper that says spring security to store a security context to a cookie.
   * 
   * @author Ashvin
   */
  final class SaveToCookieResponseWrapper extends SaveContextOnUpdateOrErrorResponseWrapper {

    private AuthenticationTrustResolver trustResolver = new AuthenticationTrustResolverImpl();

    private final HttpServletRequest request;
    private final String cookieName;
    private final String secret;
    private final int expiryDuration;

    /**
     * Instantiates {@link SaveToCookieResponseWrapper}.
     * 
     * @param request the request object (used to obtain the session, if one exists).
     * @param response the response to be wrapped.
     * @param cookieName The name of the cookie that stores security context.
     * @param secret The secret used to encrypt and decrypt cookie value.
     */
    public SaveToCookieResponseWrapper(HttpServletRequest request, HttpServletResponse response,
        final String cookieName, final String secret, final int expiryDuration) {
      super(response, false);
      this.request = request;
      this.cookieName = cookieName;
      this.secret = secret;
      this.expiryDuration = expiryDuration;
    }

    /**
     * Stores the supplied security context in the cookie (if available). <br/>
     * <br/>
     * If security context is not available or {@link AuthenticationTrustResolver} identifies the
     * current user as anonymous, then removes security context from the cookie.
     */
    @Override
    protected void saveContext(SecurityContext securityContext) {
      HttpServletResponse response = (HttpServletResponse) getResponse();
      final Authentication authentication = securityContext.getAuthentication();

      if (authentication == null || trustResolver.isAnonymous(authentication)) {
        logger.debug("SecurityContext is empty or contents are anonymous - "
            + "context will be removed from Cookie.");
        response.addCookie(getExpiringAuthenticationCookie());
        return;
      }

      // Sets/updates authentication cookie every time security content is available and user is not
      // anonymous.
      Date expiresAt = new Date(System.currentTimeMillis() + expiryDuration);
      String jwt = Jwts.builder()
                       .signWith(SignatureAlgorithm.HS512, secret)
                       .setSubject(authentication.getName())
                       .setExpiration(expiresAt)
                       .compact();
      response.addCookie(getAuthenticationCookie(jwt));

      if (logger.isDebugEnabled()) {
        logger.debug("SecurityContext '{}' stored to Cookie.", securityContext);
      }
    }

    /**
     * Returns cookie that will be stored in browser.
     */
    private Cookie getAuthenticationCookie(String cookieValue) {
      return createCookie(cookieValue, expiryDuration);
    }

    /**
     * Returns cookie that will be deleted instantly.
     */
    private Cookie getExpiringAuthenticationCookie() {
      return createCookie("", 0);
    }

    private Cookie createCookie(String cookieValue, int maxAge) {
      Cookie authenticationCookie = new Cookie(cookieName, cookieValue);
      authenticationCookie.setPath("/");
      authenticationCookie.setMaxAge(maxAge);
      authenticationCookie.setHttpOnly(true);
      authenticationCookie.setSecure(request.isSecure());
      return authenticationCookie;
    }
  }
}

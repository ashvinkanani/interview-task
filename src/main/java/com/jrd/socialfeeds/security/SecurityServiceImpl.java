package com.jrd.socialfeeds.security;

import com.jrd.socialfeeds.model.User;
import com.jrd.socialfeeds.users.UserRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

/**
 * An implementation for {@link SecurityService}.
 * 
 * @author Ashvin
 */
@Service
public class SecurityServiceImpl implements SecurityService {

  private static final Logger logger = LoggerFactory.getLogger(SecurityServiceImpl.class);

  @Autowired
  private UserRepository userRepository;

  @Override
  public User getCurrentUser() {
    logger.debug("getCurrentUser() :: Invoked");
    Authentication auth = SecurityContextHolder.getContext()
                                               .getAuthentication();
    if (auth == null) {
      return null;
    }

    String username = (String) auth.getPrincipal();
    logger.debug("getCurrentUser() :: Found. userName={}", username);
    return userRepository.findByUsername(username);
  }
}

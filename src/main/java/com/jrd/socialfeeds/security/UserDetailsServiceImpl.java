package com.jrd.socialfeeds.security;

import com.jrd.socialfeeds.users.UserRepository;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Loads logged in user's data.
 * 
 * @author Ashvin
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

  @Autowired
  private UserRepository userRepository;

  @Override
  @Transactional(readOnly = true)
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

    com.jrd.socialfeeds.model.User appUser = userRepository.findByUsername(username);

    if (appUser == null) {
      throw new UsernameNotFoundException(username);
    }

    return new User(appUser.getUsername(), appUser.getPassword(), new ArrayList<>());
  }
}

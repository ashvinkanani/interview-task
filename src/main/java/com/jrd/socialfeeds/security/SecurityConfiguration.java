package com.jrd.socialfeeds.security;

import com.jrd.socialfeeds.config.Endpoint;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * Configures spring security for web.
 */
@EnableWebSecurity
@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

  @Override
  protected void configure(HttpSecurity http) throws Exception {

    JwtSecurityContextRepository securityContextRepository =
        getApplicationContext().getBean(JwtSecurityContextRepository.class);

    // @formatter:off
    http
      // This disables storing session information in HttpSession
      .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
      .and()
        // Set out custom security context repository
        .securityContext().securityContextRepository(securityContextRepository)
      .and()
        // Set csrf token repository to use a Cookie and no HttpSession
        .csrf().disable() /*.csrfTokenRepository(new CookieCsrfTokenRepository())
      .and()*/
        .authorizeRequests()
          .antMatchers(HttpMethod.POST, Endpoint.USERS).permitAll()
          .antMatchers(HttpMethod.POST, Endpoint.LOGIN).permitAll()
          .anyRequest().authenticated();
        // @formatter:on
  }

  @Override
  public void configure(AuthenticationManagerBuilder auth) throws Exception {
    UserDetailsService userDetailsService =
        getApplicationContext().getBean(UserDetailsService.class);
    BCryptPasswordEncoder bcryptPasswordEncoder =
        getApplicationContext().getBean(BCryptPasswordEncoder.class);

    auth.userDetailsService(userDetailsService)
        .passwordEncoder(bcryptPasswordEncoder);
  }

  @Override
  @Bean(BeanIds.AUTHENTICATION_MANAGER)
  public AuthenticationManager authenticationManagerBean() throws Exception {
    return super.authenticationManagerBean();
  }

  @Bean
  public JwtSecurityContextRepository contextRepository(UserDetailsService userDetailsService,
      @Value("${securityContext.cookieName:JSESSIONID}") String cookieName,
      @Value("${securityContext.secret}") String secret,
      @Value("${securityContext.expiryDuration:3600000}") int expityDuration) {
    return new JwtSecurityContextRepository(userDetailsService, cookieName, secret, expityDuration);
  }

  @Bean
  public BCryptPasswordEncoder bcryptPasswordEncoder() {
    return new BCryptPasswordEncoder();
  }
}

package com.jrd.socialfeeds.security;

import com.jrd.socialfeeds.model.User;

/**
 * Used to hold methods related to security.
 * 
 * @author Ashvin
 */
public interface SecurityService {

  /**
   * Returns currently logged in user object.
   * 
   * @return {@link User} instance if logged in user is available.<br/>
   *         <code>null</code> if public URL is invoked and logged in user is not available.
   */
  public User getCurrentUser();
}

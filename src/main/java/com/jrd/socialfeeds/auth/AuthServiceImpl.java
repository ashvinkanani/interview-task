package com.jrd.socialfeeds.auth;

import com.jrd.socialfeeds.exception.IllegalArgumentException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class AuthServiceImpl implements AuthService {

  private static final Logger logger = LoggerFactory.getLogger(AuthService.class);

  private static final String BAD_REQUEST = "BAD_REQUEST";
  private static final String INCORRECT_CREDENTIAL = "INCORRECT_CREDENTIAL";

  @Autowired
  private AuthenticationManager authenticationManager;

  @Override
  public void login(LoginRequest loginRequest) {

    validateForLogin(loginRequest);

    try {
      Authentication authentication = authenticationManager.authenticate(
          new UsernamePasswordAuthenticationToken(loginRequest.getUsername()
                                                              .toLowerCase(),
              loginRequest.getPassword()));

      SecurityContextHolder.getContext()
                           .setAuthentication(authentication);
      logger.info("login :: Successful. username={}", loginRequest.getUsername());
    } catch (BadCredentialsException ex) {
      throw new IllegalArgumentException(INCORRECT_CREDENTIAL, "Incorrect username or password.");
    }
  }

  private void validateForLogin(LoginRequest loginRequest) {
    if (loginRequest == null) {
      throw new IllegalArgumentException(BAD_REQUEST,
          "Please, verify that 'loginRequest' is provided.");
    }

    if (StringUtils.isBlank(loginRequest.getUsername())) {
      throw new IllegalArgumentException(BAD_REQUEST, "Please, verify that 'username' is provided");
    }

    if (StringUtils.isBlank(loginRequest.getPassword())) {
      throw new IllegalArgumentException(BAD_REQUEST, "Please, verify that 'password' is provided");
    }
  }
}

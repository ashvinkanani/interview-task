package com.jrd.socialfeeds.auth;

/**
 * Provides operations related to authenticating an user. Such API includes login, logout, etc.
 * 
 * @author Ashvin
 */
public interface AuthService {

  /**
   * Logins a particular user with given 'username' in to the system. As a response, Cookie will be
   * set in user's browser.
   * 
   * @param loginRequest {@link LoginRequest}.
   * @throws IllegalArgumentException If username or password is <code>null</code>, blank or
   *         whitespace.
   * @throws IllegalArgumentException If credentials provided are not valid (i.e. user with given
   *         username is not available or password is wrong.
   */
  public void login(LoginRequest loginRequest);

}

package com.jrd.socialfeeds.auth;

import com.jrd.socialfeeds.config.BaseApiController;
import com.jrd.socialfeeds.config.Endpoint;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Provides APIs related to authenticating an user. Such API includes login, logout, etc.
 * 
 * @author Ashvin
 */
@RestController
public class AuthController extends BaseApiController {

  @Autowired
  private AuthService authService;

  /**
   * Logins a particular user with given 'username' in to the system. As a response, Cookie will be
   * set in user's browser.
   * 
   * @throws IllegalArgumentException If username or password is <code>null</code>, blank or
   *         whitespace.
   * @throws IllegalArgumentException If credentials provided are not valid (i.e. user with given
   *         username is not available or password is wrong.
   */
  @PostMapping(value = Endpoint.LOGIN, consumes = MediaType.APPLICATION_JSON_VALUE)
  public void login(@Valid @RequestBody LoginRequest loginRequest) {
    authService.login(loginRequest);
  }

}

package com.jrd.socialfeeds.model;

import java.util.UUID;

import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "socialfeeds")
@CompoundIndex(name = "idx_socialfeeds_userid_type_feedid",
    def = "{'userId':1, 'type': 1, 'feedId': -1}", unique = true)
public class SocialFeed {

  /**
   * The Unique ID.
   */
  @Id
  private String id;

  /**
   * The id of the logged in user.
   */
  @NotNull
  private String userId;

  /**
   * The social feed type. e.g. TWEET, FACEBOOK_FEEDS.
   */
  @NotNull
  private Type type;

  /**
   * The unique id of the feed provided by social site. e.g. twitter, facebook, etc.
   */
  @NotNull
  @Indexed
  private String feedId;

  /**
   * The feed text.
   */
  @NotNull
  private String text;

  /**
   * Display name of the social user.
   */
  private String displayName;

  /**
   * Social User account name.
   */
  private String internalName;

  /**
   * Feed creation time.
   */
  private Long createdAt;

  public SocialFeed() {
    this.setId(UUID.randomUUID()
                   .toString());
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public Type getType() {
    return type;
  }

  public void setType(Type type) {
    this.type = type;
  }

  public String getFeedId() {
    return feedId;
  }

  public void setFeedId(String feedId) {
    this.feedId = feedId;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }

  public String getInternalName() {
    return internalName;
  }

  public void setInternalName(String internalName) {
    this.internalName = internalName;
  }

  public Long getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Long createdAt) {
    this.createdAt = createdAt;
  }

  public static enum Type {
    TWEET, FACEBOOK_FEEDS;
  }
}

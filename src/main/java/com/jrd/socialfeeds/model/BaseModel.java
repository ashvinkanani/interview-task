package com.jrd.socialfeeds.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

import org.hibernate.Hibernate;

/**
 * This class is used as a base model for all other models. It contains common properties for all
 * models.
 */
@SuppressWarnings("rawtypes")
@MappedSuperclass
public class BaseModel<K extends Comparable> implements Serializable, Comparable<Object> {

  private static final long serialVersionUID = 602580387269695687L;

  @Id
  private K id;

  @Version
  private Integer version;

  @JsonIgnore
  public boolean isNew() {
    return version == null;
  }


  public K getId() {
    return id;
  }

  public void setId(K id) {
    this.id = id;
  }

  public Integer getVersion() {
    return version;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((this.getId() == null) ? 0
        : this.getId()
              .hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (Hibernate.getClass(this) != Hibernate.getClass(obj)) {
      return false;
    }

    BaseModel<?> baseModel = (BaseModel<?>) obj;

    if (getId() == null || baseModel.getId() == null) {
      return false;
    }
    return this.getId()
               .equals(baseModel.getId());
  }

  /**
   * Compares this object.
   */
  @SuppressWarnings("unchecked")
  public int compareTo(Object object) {
    BaseModel<K> baseModel = (BaseModel<K>) object;

    return this.getId()
               .compareTo(baseModel.getId());
  }
}

package com.jrd.socialfeeds.exception;

/**
 * This class is used to throw resource not found exception with custom code.
 * 
 * @author Ashvin
 */
public class ResourceNotFoundException extends RuntimeException {

  private static final long serialVersionUID = 643434130750093678L;

  private String code;

  public ResourceNotFoundException() {
    super();
  }

  public ResourceNotFoundException(String msg) {
    super(msg);
  }

  public ResourceNotFoundException(String code, String msg) {
    this(msg);
    this.setCode(code);
  }

  public ResourceNotFoundException(String message, Throwable cause) {
    super(message, cause);
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }
}

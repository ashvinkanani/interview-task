package com.jrd.socialfeeds.exception;

/**
 * This class is used to throw invalid state exception with custom error code.
 * 
 * @author Ashvin
 */
public class IllegalStateException extends java.lang.IllegalStateException implements IErrorInfo {

  private static final long serialVersionUID = -2264198754028331576L;

  private String code;

  public IllegalStateException(String code, String msg) {
    super(msg);
    this.code = code;
  }

  public IllegalStateException(String message, Throwable cause) {
    super(message, cause);
  }

  public String getCode() {
    return code;
  }
}

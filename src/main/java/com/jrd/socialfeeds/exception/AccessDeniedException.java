package com.jrd.socialfeeds.exception;

/**
 * This class is used to throw access denied exception with custom error code.
 * 
 * @author Ashvin
 */
public class AccessDeniedException extends RuntimeException implements IErrorInfo {

  private static final long serialVersionUID = 8453166699525097313L;

  private String code;

  public AccessDeniedException(String code, String msg) {
    super(msg);
    this.code = code;
  }

  public AccessDeniedException(String message, Throwable cause) {
    super(message, cause);
  }

  public String getCode() {
    return code;
  }
}

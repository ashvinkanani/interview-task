package com.jrd.socialfeeds.exception;

/**
 * This class is used to throw authentication related exception with custom error code.
 * 
 * @author Ashvin
 */
public class AuthenticationException extends RuntimeException implements IErrorInfo {

  private static final long serialVersionUID = 539291463963390429L;

  private String code;

  public AuthenticationException(String code, String msg) {
    super(msg);
    this.code = code;
  }

  public AuthenticationException(String message, Throwable cause) {
    super(message, cause);
  }

  public String getCode() {
    return code;
  }
}

package com.jrd.socialfeeds.exception;

/**
 * This class is used to throw invalid argument exception with custom error code.
 * 
 * @author Ashvin
 */
public class IllegalArgumentException extends java.lang.IllegalArgumentException
    implements IErrorInfo {

  private static final long serialVersionUID = -7790690968344842842L;

  private String code;

  public IllegalArgumentException(String code, String msg) {
    super(msg);
    this.code = code;
  }

  public IllegalArgumentException(String message, Throwable cause) {
    super(message, cause);
  }

  public String getCode() {
    return code;
  }
}

package com.jrd.socialfeeds.exception;

/**
 * This interface is used to provide custom error code for any exception.
 * 
 * @author Ashvin
 */
public interface IErrorInfo {

  String getCode();

}

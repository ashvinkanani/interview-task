package com.jrd.socialfeeds.config;

/**
 * Contains API endpoints.
 * 
 * @author Ashvin
 */
public class Endpoint {

  public static final String TEST = "/test";

  // Users
  public static final String USERS = "/users"; // POST

  // Auth
  public static final String LOGIN = "/login"; // POST

  // Social feeds
  public static final String FETCH_FEEDS = "/feeds/{service}"; // GET
  public static final String LIST_FEEDS = "/feeds"; // GET

  // Socia feeds - facebook
  public static final String FACEBOOK_GET_AUTH_URL = "/facebook/generateauthurl"; // GET
  public static final String FACEBOOK_CALLBACK = "/facebook/callback"; // GET
}

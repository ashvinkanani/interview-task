
-- -----------------------------------------------------
-- Table `user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `user` (
  `id` VARCHAR(36) NOT NULL,
  `version` BIGINT NOT NULL,
  `first_name` VARCHAR(250) NULL,
  `last_name` VARCHAR(250) NULL,
  `username` VARCHAR(250) NOT NULL,
  `password` VARCHAR(250) NOT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `un_user_username` (`username` ASC))
ENGINE = InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_bin;
